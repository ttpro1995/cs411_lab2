#pragma once
#include <vector>
#include "Constants.h"

class DrawObj
{
private:
	int type;
	int begin_x;
	int begin_y;
	int end_x;
	int end_y;
public:
	
	DrawObj(int type, int begin_x, int begin_y, int cur_x, int cur_y);
	void draw();

};

