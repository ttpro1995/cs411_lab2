#pragma once
struct Point
{
	int x;
	int y;
	Point(int x, int );
	static Point midpoint(Point a, Point b);
	void draw();
	bool isInside();
};

