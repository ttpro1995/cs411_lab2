#pragma once
#include <vector>
#include "DrawObj.h"
class Draw
{
private:
	std::vector<DrawObj> objList;
	static const float scale_unit;
public:
	Draw();
	void addObj(DrawObj mObj);
	void drawAllObs();
	void drawWhenDrag(int type, int begin_x, int begin_y, int end_x, int end_y);
	static void putpixel(float x, float y);
	static void drawCircleWithMidpoint(float x, float y, float r);
	static void drawEllipseWithMidpoint(float x0, float y0, float rx, float ry);
	~Draw();
};

