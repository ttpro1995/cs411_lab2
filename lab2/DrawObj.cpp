#include "DrawObj.h"
#include <GL\glew.h>
#include<GL\freeglut.h>
#include <iostream>
#include "Draw.h"
#include "Point.h"




DrawObj::DrawObj(int type, int begin_x, int begin_y, int end_x, int end_y)
{
	this->begin_x = begin_x;
	this->begin_y = begin_y;
	this->end_x = end_x;
	this->end_y = end_y;
		this->type = type;
}

void DrawObj::draw()
{
	switch (type)
	{
	case DUONG_THANG: {
		glBegin(GL_LINES);
		glVertex2f(begin_x, begin_y);
		glVertex2f(end_x, end_y);
		glEnd();
		break;
	}

	case TAM_GIAC_VUONG_CAN: {
		
		
		glBegin(GL_LINE_LOOP);
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x < length_y)
			a = length_x;
		else
			a = length_y;
		int horizontal_a;
		if (end_x - begin_x >0) 
			horizontal_a = a;
		else horizontal_a = -a;

		int vertical_a;
		if (end_y - begin_y > 0)
			vertical_a = a;
		else vertical_a = -a;


		glVertex2f(begin_x, begin_y);
		glVertex2f(begin_x, begin_y + vertical_a);
		glVertex2f(begin_x + horizontal_a, begin_y + vertical_a);
		glEnd();
		break;
	}

	case TAM_GIAC_DEU: {
		glBegin(GL_LINE_LOOP);
		float h = abs(begin_y - end_y);
		// h  = a*sqrt(3)/2 => a/2 = h/sqrt(3)
		float half_a_f = h / sqrt(3);
		int half_a = round(half_a_f);
		glVertex2f(begin_x, begin_y);
		glVertex2f(begin_x - half_a, end_y);
		glVertex2f(begin_x + half_a, end_y);
		glEnd();
		break;
	}

	case HINH_CHU_NHAT: {
		glBegin(GL_LINE_LOOP);
		glVertex2f(begin_x, begin_y);
		glVertex2f(begin_x, end_y);
		glVertex2f(end_x, end_y);
		glVertex2f(end_x, begin_y);
		glEnd();
		break;
	}

	case HINH_VUONG: {
		glBegin(GL_LINE_LOOP);
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x < length_y)
			a = length_x;
		else
			a = length_y;
		int horizontal_a;
		if (begin_x - end_x < 0)
			horizontal_a = a;
		else
			horizontal_a = -a;

		int vertical_a;
		if (begin_y - end_y < 0)
			vertical_a = a;
		else
			vertical_a = -a;
		glVertex2f(begin_x, begin_y);
		glVertex2f(begin_x, begin_y + vertical_a);
		glVertex2f(begin_x + horizontal_a, begin_y + vertical_a);
		glVertex2f(begin_x + horizontal_a, begin_y);
		glEnd();
		break;
	}
	case HINH_TRON: {
		int x = (begin_x + end_x) / 2;
		int y = (begin_y + end_y) / 2;
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x > length_y)
			a = length_x;
		else
			a = length_y;

		glBegin(GL_POINTS);
		Draw::drawCircleWithMidpoint(x, y, a);
		glEnd();
		break;
	}

	case ELIP: {
		int x = (begin_x + end_x) / 2;
		int y = (begin_y + end_y) / 2;
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		

		glBegin(GL_POINTS);
		Draw::drawEllipseWithMidpoint(x, y, length_x,length_y);
		glEnd();
		break;
	}

	case NGU_GIAC_DEU: {
		const double PI = 3.14159265358979323846;
		//(x2-x1)^2 + (y2-y1)^2
		double r = (end_x - begin_x)*(end_x - begin_x) + (end_y - begin_y)*(end_y - begin_y);
		r = sqrt(r);
		int sides = 5;

		glBegin(GL_LINE_LOOP);
		for (int i = 0; i<sides; i++) {
			double angle = i * 2 * PI / sides;
			glVertex2d(begin_x + r*cos(angle), begin_y + r*sin(angle));
		}
		glEnd();
		break;
	}

	case LUC_GIAC_DEU: {
		const double PI = 3.14159265358979323846;
		//(x2-x1)^2 + (y2-y1)^2
		double r = (end_x - begin_x)*(end_x - begin_x) + (end_y - begin_y)*(end_y - begin_y);
		r = sqrt(r);
		int sides = 6;

		glBegin(GL_LINE_LOOP);
		for (int i = 0; i<sides; i++) {
			double angle = i * 2 * PI / sides;
			glVertex2d(begin_x + r*cos(angle), begin_y + r*sin(angle));
		}
		glEnd();
		break;
	}

	case CONG: {
		
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x < length_y)
			a = length_x;
		else
			a = length_y;
		int horizontal_a;
		if (begin_x - end_x < 0)
			horizontal_a = a;
		else
			horizontal_a = -a;

		int vertical_a;
		if (begin_y - end_y < 0)
			vertical_a = a;
		else
			vertical_a = -a;
		//glVertex2f(begin_x, begin_y);
		//glVertex2f(begin_x, begin_y + vertical_a);
		//glVertex2f(begin_x + horizontal_a, begin_y + vertical_a);
		//glVertex2f(begin_x + horizontal_a, begin_y);
		
 a = a / 8;

		Point up = Point::midpoint(Point(begin_x, begin_y), Point(begin_x + horizontal_a, begin_y));
		Point down = Point::midpoint(Point(begin_x, begin_y + vertical_a), Point(begin_x + horizontal_a, begin_y + vertical_a));
		Point left = Point::midpoint(Point(begin_x, begin_y), Point(begin_x, begin_y + vertical_a));
		Point right = Point::midpoint(Point(begin_x+horizontal_a, begin_y), Point(begin_x + horizontal_a, begin_y+vertical_a));
		glBegin(GL_LINE_LOOP);
		glVertex2f(up.x-a,up.y);
		glVertex2f(up.x+a,up.y);
		glVertex2f(up.x + a, right.y +a);
		glVertex2f(right.x, right.y+a);
		glVertex2f(right.x, right.y - a);
		glVertex2f(down.x +a, right.y-a);
		glVertex2f(down.x +a, down.y);
		glVertex2f(down.x -a, down.y);
		glVertex2f(down.x -a, left.y-a);
		glVertex2f(left.x,left.y-a);
		glVertex2f(left.x, left.y + a);
		glVertex2f(up.x - a, left.y+a);
		glEnd();
		
		break;
	}

	case TRU: {
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x < length_y)
			a = length_x;
		else
			a = length_y;
		int horizontal_a;
		if (begin_x - end_x < 0)
			horizontal_a = a;
		else
			horizontal_a = -a;

		int vertical_a;
		if (begin_y - end_y < 0)
			vertical_a = a;
		else
			vertical_a = -a;
		//glVertex2f(begin_x, begin_y);
		//glVertex2f(begin_x, begin_y + vertical_a);
		//glVertex2f(begin_x + horizontal_a, begin_y + vertical_a);
		//glVertex2f(begin_x + horizontal_a, begin_y);

		a = a / 8;

		Point up = Point::midpoint(Point(begin_x, begin_y), Point(begin_x + horizontal_a, begin_y));
		Point down = Point::midpoint(Point(begin_x, begin_y + vertical_a), Point(begin_x + horizontal_a, begin_y + vertical_a));
		Point left = Point::midpoint(Point(begin_x, begin_y), Point(begin_x, begin_y + vertical_a));
		Point right = Point::midpoint(Point(begin_x + horizontal_a, begin_y), Point(begin_x + horizontal_a, begin_y + vertical_a));
		glBegin(GL_LINE_LOOP);

		glVertex2f(right.x, right.y + a);
		glVertex2f(right.x, right.y - a);
		//glVertex2f(down.x + a, right.y - a);
		//glVertex2f(down.x + a, down.y);
		//glVertex2f(down.x - a, down.y);
		//glVertex2f(down.x - a, left.y - a);
		glVertex2f(left.x, left.y - a);
		glVertex2f(left.x, left.y + a);
		//glVertex2f(up.x - a, left.y + a);
		glEnd();
		break;
	}
	case NHAN: {
		glBegin(GL_LINE_LOOP);
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x < length_y)
			a = length_x;
		else
			a = length_y;
		
		a = a / 8;
		Point A(begin_x + 2*a, begin_y);
		Point B(begin_x + 4 * a, begin_y + 2 * a);
		Point C(begin_x + 6 * a, begin_y);
		Point D(begin_x + 8 * a, begin_y + 2 * a);
		Point E(begin_x + 6 * a, begin_y + 4 * a);
		Point F(begin_x + 8 * a, begin_y + 6 * a);
		Point G(begin_x + 6 * a, begin_y + 8 * a);
		Point H(begin_x + 4 * a, begin_y + 6 * a);
		Point I(begin_x + 2 * a, begin_y + 8 * a);
		Point K(begin_x, begin_y + 6 * a);
		Point L(begin_x + 2 * a, begin_y + 4 * a);
		Point M(begin_x, begin_y + 2 * a);
		A.draw();
		B.draw();
		C.draw();
		D.draw();
		E.draw();
		F.draw();
		G.draw();
		H.draw();
		I.draw();
		K.draw();
		L.draw();
		M.draw();
		glEnd();
		break;
	}

	case CHIA: {
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x < length_y)
			a = length_x;
		else
			a = length_y;
		
		a = a / 8;

		Point M(begin_x, begin_y + 3 * a);
		Point N(begin_x, begin_y + 5 * a);
		Point Q(begin_x + 8*a, begin_y + 5 * a);
		Point P(begin_x + 8*a, begin_y + 3 * a);

		glBegin(GL_LINE_LOOP);
		M.draw();
		N.draw();
		Q.draw();
		P.draw();
		glEnd();

		glBegin(GL_POINTS);
		Point A(begin_x + 4 * a, begin_y+a);
		Point B(begin_x + 4 * a, begin_y + 7 * a);
		Draw::drawCircleWithMidpoint(A.x, A.y, a);
		Draw::drawCircleWithMidpoint(B.x, B.y, a);
		glEnd();
		break;
	}

	case MUI_TEN: {
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x < length_y)
			a = length_x;
		else
			a = length_y;

		a = a / 8;

		Point M(begin_x, begin_y + 3 * a);
		Point N(begin_x, begin_y + 5 * a);
		Point A(begin_x + 6 * a, begin_y + 5 * a);
		Point B(begin_x + 6 * a, begin_y + 6 * a);
		Point C(begin_x + 8 * a, begin_y + 4 * a);
		Point D(begin_x + 6 * a, begin_y + 2 * a);
		Point E(begin_x + 6 * a, begin_y + 3 * a);
		glBegin(GL_LINE_LOOP);
		M.draw();
		N.draw();
		A.draw();
		B.draw();
		C.draw();
		D.draw();
		E.draw();
		glEnd();
		break;
	}

	case NGOI_SAO: {
		int length_x = abs(begin_x - end_x);
		int length_y = abs(begin_y - end_y);
		int a;
		if (length_x < length_y)
			a = length_x;
		else
			a = length_y;

		a = a / 8;

		int x = begin_x;
		int y = begin_y;
		
		Point A(x, y + 6 * a);
		Point B(x + 3 * a, y + 5 * a);
		Point C(x + 4 * a, y + 8 * a);
		Point D(x + 5 * a, y + 5 * a);
		Point E(x + 8 * a, y + 6 * a);
		Point F(x + 6 * a, y + 3 * a);
		Point G(x + 6 * a, y);
		Point H(x + 4 * a, y + 3 * a);
		Point I(x + 2 * a, y);
		Point K(x + 2 * a, y + 3 * a);

		glBegin(GL_LINE_LOOP);
		A.draw();
		B.draw();
		C.draw();
		D.draw();
		E.draw();
		F.draw();
		G.draw();
		H.draw();
		I.draw();
		K.draw();
		glEnd();
		break;
	}
		
	}// end of switch statement

	}
	


