#include "RGBColor.h"




bool RGBColor::operator==(const RGBColor & d) const
{
	if (this->r == d.r)
		if (this->g == d.g)
			if (this->b == d.b)
				return true;
	return false;
}

bool RGBColor::operator!=(const RGBColor & d) const
{
	if (this->r == d.r)
		if (this->g == d.g)
			if (this->b == d.b)
				return false;
	return true;
}

RGBColor::RGBColor(unsigned char r, unsigned char g, unsigned char b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}



RGBColor::~RGBColor()
{
}
