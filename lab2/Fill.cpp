#include "Fill.h"
#include "DrawObj.h"
#include <GL\glew.h>
#include<GL\freeglut.h>
#include "Constants.h"
#include "RGBColor.h"
#include "Point.h"
#include <iostream>
#include <string>
#include <stack>
#include <queue>
RGBColor Fill::getPixel(int x, int y)
{
	unsigned char *ptr = new unsigned char[3];
	glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, ptr);
	RGBColor color(ptr[0], ptr[1], ptr[2]);
	return color;
}

void Fill::putPixel(int x, int y, RGBColor color)
{
	unsigned char * ptr = new unsigned char[3];
	ptr[0] = color.r;
	ptr[1] = color.g;
	ptr[2] = color.b;
	glRasterPos2i(x, y);
	glDrawPixels(1, 1, GL_RGB, GL_UNSIGNED_BYTE, ptr);
	glFlush();
}


void Fill::floodFill(int x, int y, RGBColor fill_color)
{
	Point seed(x, y);
	std::stack<Point> the_stack;
	the_stack.push(seed);
	RGBColor seedColor = getPixel(seed.x,seed.y);

	//std::cout << "cur color = " << curColor.r << " " << curColor.g << " " << curColor.b << "\n";
	while (!the_stack.empty())
	{
		Point curPoint = the_stack.top();
		the_stack.pop();
		//std::cout << "pop " << curPoint.x << " " << curPoint.y<<"\n ";
		RGBColor curColor = getPixel(curPoint.x, curPoint.y);
		if (curColor == seedColor && curPoint.isInside()) {
			putPixel(curPoint.x, curPoint.y, fill_color);
			Point left(curPoint.x - 1, curPoint.y);
			Point top(curPoint.x, curPoint.y + 1);
			Point right(curPoint.x + 1, curPoint.y);
			Point bottom(curPoint.x, curPoint.y - 1);
			the_stack.push(left);
			the_stack.push(top);
			the_stack.push(right);
			the_stack.push(bottom);
		}
	}
}

void Fill::boundaryFill(int x, int y, RGBColor fill_color, RGBColor boundary_color)
{
	Point seed(x, y);
	std::stack<Point> the_stack;
	the_stack.push(seed);

	//std::cout << "cur color = " << curColor.r << " " << curColor.g << " " << curColor.b << "\n";
	while (!the_stack.empty())
	{
		Point curPoint = the_stack.top();
		the_stack.pop();
		//std::cout << "pop " << curPoint.x << " " << curPoint.y<<"\n ";
		RGBColor curColor = getPixel(curPoint.x, curPoint.y);
		if (curColor != boundary_color && curColor != fill_color && curPoint.isInside()) {
			putPixel(curPoint.x, curPoint.y, fill_color);
			Point left(curPoint.x - 1, curPoint.y);
			Point top(curPoint.x, curPoint.y + 1);
			Point right(curPoint.x + 1, curPoint.y);
			Point bottom(curPoint.x, curPoint.y - 1);
			the_stack.push(left);
			the_stack.push(top);
			the_stack.push(right);
			the_stack.push(bottom);
		}
	}
}

void Fill::fillMouseClick(int x, int y, int type)
{
	GLuint64 startTime, stopTime;
	unsigned int queryID[2];
	std::string name = "";
	// generate two queries
	glGenQueries(2, queryID);
	glQueryCounter(queryID[0], GL_TIMESTAMP);
	
	RGBColor boundaryColor(255, 255, 255);
	switch (type)
	{
	case XANH: {
		RGBColor color(0, 0, 255);
		name = "scan line ";
		scanLineFill(x, y, color, boundaryColor);
		break;
	}
	case DO: {
		RGBColor color(255, 0, 0);
		name = "boundary fill ";
		boundaryFill(x, y, color, boundaryColor);
		//scanLineFill(x, y, color, boundaryColor);
		break;
	}
	case VANG: {
		RGBColor color(255, 255, 0);
		name = "flood fill ";
		floodFill(x, y, color);
		break;
	}
	}// end of switch case
	glQueryCounter(queryID[1], GL_TIMESTAMP);
	// wait until the results are available
	GLint stopTimerAvailable = 0;
	while (!stopTimerAvailable) {
		glGetQueryObjectiv(queryID[1],
			GL_QUERY_RESULT_AVAILABLE,
			&stopTimerAvailable);
	}

	// get query results
	glGetQueryObjectui64v(queryID[0], GL_QUERY_RESULT, &startTime);
	glGetQueryObjectui64v(queryID[1], GL_QUERY_RESULT, &stopTime);
	std::cout << name;
	printf(" in : %f ms\n", (stopTime - startTime) / 1000000.0);
}

void Fill::scanLineFill(int x, int y, RGBColor fill_color, RGBColor boundary_color)
{
	int cur_y = y;
	while (scan_a_line(cur_y, fill_color, boundary_color)) {
		//std::cout << "y = " << cur_y << "\n";
		cur_y += 1;
	}
	cur_y = y;
	while (scan_a_line(cur_y, fill_color, boundary_color)) {
		//std::cout << "y = " << cur_y << "\n";
		cur_y -= 1;
	}
}

bool Fill::scan_a_line(int y, RGBColor fill_color, RGBColor boundary_color)
{
	std::queue<int> mQueue;
	for (int i = 1; i < windows_width; i++) {
		RGBColor color = getPixel(i, y);
		if (color == boundary_color)
		//if (!isPeak(i, y, fill_color,boundary_color))
		mQueue.push(i);
	}

	if (mQueue.empty())
		return false;
	
	while (mQueue.size() >= 2) {
		int begin_x = mQueue.front();
		mQueue.pop();
		int end_x = mQueue.front();
		mQueue.pop();
		for (int i = begin_x+1; i < end_x; i++) {
			putPixel(i, y, fill_color);
		}
	}
	return true;
}

bool Fill::isPeak(int x, int y, RGBColor fill_color, RGBColor boundary_color)
{
	bool top = false;
	bool bot = false;

	RGBColor t1 = getPixel(x + 1, y + 1);
	RGBColor t2 = getPixel(x , y + 1);
	RGBColor t3 = getPixel(x - 1, y + 1);
	if (t1 == boundary_color || t2 == boundary_color || t3 == boundary_color)
		top = true;
	RGBColor b1 = getPixel(x + 1, y - 1);
	RGBColor b2 = getPixel(x , y - 1);
	RGBColor b3 = getPixel(x - 1, y - 1);
	if (b1 == boundary_color || b2 == boundary_color || b3 == boundary_color)
		bot = true;

	if (!bot || !top)
		return true;

	return false;
}

Fill::Fill()
{
}


Fill::~Fill()
{
}
