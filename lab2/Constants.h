#pragma once

static int windows_width = 500;
static int windows_height = 500;

	static int TAM_GIAC_id;
	static int TU_GIAC_id;
	static int OVAL_id;
	static int DA_GIAC_DEU_id;
	static int HINH_KHAC_id;
	static int DAU_id;
	static int TO_MAU_id;
	static int CHON_HINH_id;
	static int MAIN_MENU_id;
	
	
	static int menu_value;

	static const int DUONG_THANG = 1;

	static const int TAM_GIAC_DEU = 2;
	static const int TAM_GIAC_VUONG_CAN = 3;
	
	static const int MUI_TEN = 4;
	static const int NGOI_SAO = 5;

	static const int HINH_TRON = 6;
	static const int ELIP = 7;

	static const int XANH = 8;//0, 0, 255
	static const int DO = 9;////255, 0, 0
	static const int VANG = 10;

	static const int HINH_CHU_NHAT = 11;
	static const int HINH_VUONG = 12;

	static const int LUC_GIAC_DEU = 13;
	static const int NGU_GIAC_DEU = 14;

	static const int CONG = 15;
	static const int TRU = 16;
	static const int NHAN = 17;
	static const int CHIA = 18;