#pragma once
#include "RGBColor.h"
class Fill
{
public:
	RGBColor getPixel(int x, int y);
	void putPixel(int x, int y, RGBColor color);
	void boundaryFill(int x, int y, RGBColor fill_color, RGBColor boundary_color);
	void fillMouseClick(int x, int y, int type);
	void floodFill(int x, int y, RGBColor fill_color);
	void scanLineFill(int x, int y, RGBColor fill_color, RGBColor boundary_color);
	bool scan_a_line(int y, RGBColor fill_color, RGBColor boundary_color);
	bool isPeak(int x, int y, RGBColor fill_color, RGBColor boundary_color);
	Fill();
	~Fill();
	
};

