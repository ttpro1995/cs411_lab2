#include "Draw.h"
#include <vector>
#include <iterator>
#include <cmath>
#include <GL\freeglut.h>
const float Draw::scale_unit = 1;
Draw::Draw()
{

}

void Draw::addObj(DrawObj mObj)
{
	objList.push_back(mObj);
}

void Draw::drawAllObs()
{
	using namespace std;
	vector <DrawObj>::iterator pos;
	for (pos = objList.begin(); pos != objList.end(); pos++) {
		pos->draw();
	}
}

void Draw::drawWhenDrag(int type, int begin_x, int begin_y, int end_x, int end_y)
{
	DrawObj tmp(type, begin_x, begin_y, end_x, end_y);
	tmp.draw();

}


Draw::~Draw()
{
}

void Draw::putpixel(float x, float y) {
	glVertex3f(x, y, 0.0f);
}

void Draw::drawCircleWithMidpoint(float x0, float y0, float r) {
	glColor3f(1.0f, 1.0f, 1.0f);

	float x = r;
	float y = 0;


	// decision = 2[(x*x + y*y - r*r ) + (2y+1)] + 1 - 2x

	// 2[(r*r + 0*0 - r*r)+ (2*0 +1)] + 1 - 2*r = 3 - 2*r
	float decision = 3 - 2 * x;   // Decision criterion divided by 2 evaluated at x=r, y=0

	while (y <= x)
	{
		putpixel(x + x0, y + y0); // Octant 1
		putpixel(y + x0, x + y0); // Octant 2
		putpixel(-y + x0, x + y0); // Octant 3
		putpixel(-x + x0, y + y0); // Octant 4
		putpixel(-x + x0, -y + y0); // Octant 5
		putpixel(-y + x0, -x + y0); // Octant 6
		putpixel(x + x0, -y + y0); // Octant 7
		putpixel(y + x0, -x + y0); // Octant 8
		y += scale_unit;
		if (decision <= 0)
		{
			/*
			decision = f(x, y+1) - f(x, y) - 4 ( we -4 because new y is already ++)
			*/
			decision += 4 * y + (6 - 4)* scale_unit;   // Change in decision criterion for y -> y+1
		}
		else
		{

			/*
			decision = f(x - 1, y+1) - f(x, y) - 4 ( we -8 because new y, x have been increase/decrease)
			*/
			x -= scale_unit;
			decision += 4 * (y - x) + (10 - 8)* scale_unit;   // Change for y -> y+1, x -> x-1
		}
	}
}


void Draw::drawEllipseWithMidpoint(float x0, float y0, float rx, float ry) {
	glColor3f(1.0f, 1.0f, 1.0f);

	float x = 0;
	float y = ry;

	// decision = ry^2 - rx^2 * ry + (1/4) * rx^2
	float decision = ry*ry - rx*rx*ry + 0.25*rx*rx;
	while (2 * ry*ry*x < 2 * rx*rx*y)
	{
		putpixel(x + x0, y + y0);
		putpixel(-x + x0, y + y0);
		putpixel(x + x0, -y + y0);
		putpixel(-x + x0, -y + y0);



		if (decision < 0) {
			x += scale_unit;
			decision += 2 * ry*ry*x + ry*ry;
		}
		else {
			x += scale_unit;
			y -= scale_unit;
			decision += 2 * ry*ry*x - 2 * rx*rx*y + ry*ry;
		}
	}

	decision = 0;

	while (y>0) {
		putpixel(x + x0, y + y0);
		putpixel(-x + x0, y + y0);
		putpixel(x + x0, -y + y0);
		putpixel(-x + x0, -y + y0);

		if (decision > 0) {
			y -= scale_unit;
			decision += -2 * rx*rx*y + rx*rx;
		}
		else {
			x += scale_unit;
			y -= scale_unit;
			decision += 2 * ry*ry*x - 2 * rx*rx*y + rx*rx;
		}
	}
}