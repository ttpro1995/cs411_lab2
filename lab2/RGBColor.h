#pragma once
class RGBColor
{
public:
	unsigned char r;
	unsigned char g;
	unsigned char b;
	bool operator==(const RGBColor & d) const;
	bool operator!=(const RGBColor & d) const;
	RGBColor(unsigned char r, unsigned char g, unsigned char b);
	~RGBColor();
};

