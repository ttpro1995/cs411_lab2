﻿/*
Thai Thien
1351040

*/

#include <GL\glew.h>
#include<GL\freeglut.h>

#include "Constants.h"
#include "Draw.h"
#include "Fill.h"

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

using namespace std;
void readfile();
void menu(int value);
void createMenu(void);
void outputCurrentCommand();
int cur_x;
int cur_y;
int begin_x;
int begin_y;
int is_mouse_down;
bool is_draw = false;
bool is_fill = false;
Draw drawEngine;
Fill fillEngine;

void display(void)
{
	/* clear all pixels */
	glClear(GL_COLOR_BUFFER_BIT);


	//glColor3f​(​ 1.0f​, ​ ​ 1.0f​, ​ ​ 1.0f​); ​ ​ //Defining color (white)
	//glBegin​(​ GL_LINE_LOOP​);
	//glVertex3f​(​5.0f​, ​ ​ 5.0f​, ​ ​ 0.0f​);
	//glVertex3f​(​25.0f​, ​ ​ 5.0f​, ​ ​ 0.0f​);
	//glVertex3f​(​25.0f​, ​ ​ 25.0f​, ​ ​ 0.0f​);
	//glEnd​();
	//glFlush​();
	
	glColor3f(1.0f, 1.0f, 1.0f);
	//glBegin(GL_LINES);
	//glVertex2f(1.0f, 1.0f);
	//glVertex2f(400.0f, 400.0f);
	//glVertex2f(200.0f, 400.0f);
	glEnd();

	// on_drag render
	if (is_draw) {
		drawEngine.drawWhenDrag(menu_value, begin_x, begin_y, cur_x, cur_y);

		
	}
	drawEngine.drawAllObs();


	glFlush();
	
	//readfile();
}






void init(void)

{
	/*
	int* arg = new int[4];
	arg[0] = 1;
	arg[1] = 1;
	arg[2] = 400;
	arg[3] = 400;
	DrawObj testLine(DUONG_THANG, arg);
	drawEngine.addObj(testLine);
	*/

	//set is mouse down = 0
	is_mouse_down = 0;


	/* select clearing color */

	glClearColor(0, 0, 0, 0.0);

	/* initialize viewing values */

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	//glOrtho(0.0, 30.0, 0.0, 35.0, -1.0, 1.0);
	gluOrtho2D(0, windows_width, 0, windows_height);
}

void mouseFunc(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {
			is_draw = true;
			cur_x = x;
			cur_y = windows_height - y;
			begin_x = x;
			begin_y = windows_height - y;
		}
		else {
			if (is_draw &&  !is_fill) {
				// render after release the mouse
				is_draw = false;
				DrawObj mObj(menu_value, begin_x, begin_y, cur_x, cur_y);
				drawEngine.addObj(mObj);
				std::cout << "Add obj " << menu_value << " " << begin_x << " " << begin_y << " " << cur_x << " " << cur_y << "\n";
				glutPostRedisplay();//render now
			}
			else if (is_draw && is_fill) {
				is_draw = false;
				fillEngine.fillMouseClick(x, windows_height - y, menu_value);
			}
		}
	}
}

void ActivMouseMove(int x, int y)
{
	//Chi cap nhat thong tin khi chuot trai duoc nhan keo
	//Ham activMouseMove cung duoc goi khi chuot phai duoc nhan keo

		cur_x = x;
		cur_y = windows_height - y;
		//cout << "x = " << cur_x << endl;;
		//cout << "y = " << cur_y << endl;
	glutPostRedisplay();
}


int main(int argc, char** argv)

{
	

	glutInit(&argc, argv);



	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(windows_width, windows_height);

	glutInitWindowPosition(100, 100);
	glutInitContextVersion(1, 5);
	glutCreateWindow("1351040");
	createMenu();
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	init();



	glutDisplayFunc(display);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(ActivMouseMove);
	glutMainLoop();

	return 0; /* ANSI C requires main to return an int. */

}


void createMenu() {

	// TAM GIAC
	TAM_GIAC_id = glutCreateMenu(menu);
	//submenu TAM GIAC
	// Add sub menu entry
	glutAddMenuEntry("VUONG CAN", TAM_GIAC_VUONG_CAN);
	glutAddMenuEntry("DEU", TAM_GIAC_DEU);

	// TAM GIAC
	TU_GIAC_id = glutCreateMenu(menu);
	//submenu TAM GIAC
	// Add sub menu entry
	glutAddMenuEntry("HINH CHU NHAT", HINH_CHU_NHAT);
	glutAddMenuEntry("HINH VUONG", HINH_VUONG);

	// TAM GIAC
	OVAL_id = glutCreateMenu(menu);
	//submenu TAM GIAC
	// Add sub menu entry
	glutAddMenuEntry("HINH TRON", HINH_TRON);
	glutAddMenuEntry("ELIP", ELIP);

	// TAM GIAC
	DA_GIAC_DEU_id = glutCreateMenu(menu);
	//submenu TAM GIAC
	// Add sub menu entry
	glutAddMenuEntry("NGU GIAC DEU", NGU_GIAC_DEU);
	glutAddMenuEntry("LUC GIAC DEU", LUC_GIAC_DEU);

	// TAM GIAC
	HINH_KHAC_id = glutCreateMenu(menu);
	//submenu TAM GIAC
	// Add sub menu entry
	glutAddMenuEntry("MUI TEN", MUI_TEN);
	glutAddMenuEntry("NGOI SAO", NGOI_SAO);

	// TAM GIAC
	DAU_id = glutCreateMenu(menu);
	//submenu TAM GIAC
	// Add sub menu entry
	glutAddMenuEntry("CONG", CONG);
	glutAddMenuEntry("TRU", TRU);
	glutAddMenuEntry("NHAN", NHAN);
	glutAddMenuEntry("CHIA", CHIA);


	// TAM GIAC
	TO_MAU_id = glutCreateMenu(menu);
	//submenu TAM GIAC
	// Add sub menu entry
	glutAddMenuEntry("XANH", XANH);
	glutAddMenuEntry("DO", DO);
	glutAddMenuEntry("VANG", VANG);

	// Create the main menu
	MAIN_MENU_id = glutCreateMenu(menu);
	// Create an entry for main menu
	glutAddMenuEntry("DUONG THANG", DUONG_THANG);//one entry
	glutAddSubMenu("TAM GIAC", TAM_GIAC_id);// one entry to submenu
	glutAddSubMenu("TU GIAC", TU_GIAC_id);// one entry to submenu
	glutAddSubMenu("OVAL", OVAL_id);// one entry to submenu
	glutAddSubMenu("DA GIAC DEU", DA_GIAC_DEU_id);// one entry to submenu
	glutAddSubMenu("HINH KHAC", HINH_KHAC_id);// one entry to submenu
	glutAddSubMenu("DAU", DAU_id);// one entry to submenu
	glutAddSubMenu("TO MAU", TO_MAU_id);// one entry to submenu


	// Let the menu respond on the right mouse button
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void menu(int val) {
	menu_value = val;
	if (menu_value == XANH || menu_value == DO || menu_value == VANG)
		is_fill = true;
	else
		is_fill = false;
	outputCurrentCommand();
	// you would want to redraw now
	glutPostRedisplay();
}

void outputCurrentCommand() {
	switch (menu_value) {
	case DUONG_THANG: {
		std::cout << "DUONG THANG " << "\n";
		break;
	}

	case TAM_GIAC_VUONG_CAN: {
		std::cout << "TAM GIAC VUONG CAN "<<"\n";
		break;
	}
	case TAM_GIAC_DEU: {
		std::cout << "TAM GIAC DEU " << "\n";
		break;
	}
	case HINH_TRON: {
		std::cout << "HINH TRON " << "\n";
		break;
	}
	case ELIP: {
		std::cout << "ELIP " << "\n";
		break;
	}
	case MUI_TEN: {
		std::cout << "MUI TEN " << "\n";
		break;
	}
	case NGOI_SAO: {
		std::cout << "NGOI SAO" << "\n";
		break;
	}
	case XANH: {
		std::cout << "TO MAU XANH" << "\n";
		break;
	}
	case DO: {
		std::cout << "TO MAU DO" << "\n";
		break;
	}
	case VANG: {
		std::cout << "TO MAU VANG" << "\n";
		break;
	}
	case HINH_CHU_NHAT: {
		std::cout << "HINH CHU NHAT" << "\n";
		break;
	}

	case HINH_VUONG: {
		std::cout << "HINH_VUONG" << "\n";
		break;
	}
	
	case NGU_GIAC_DEU: {
		std::cout << "NGU_GIAC_DEU" << "\n";
		break;
	}
	case LUC_GIAC_DEU: {
		std::cout << "LUC_GIAC_DEU" << "\n";
		break;
	}
	case CONG: {
		std::cout << "CONG" << "\n";
		break;
	}
	case TRU: {
		std::cout << ":" << "\n";
		break;
	}
	case NHAN: {
		std::cout << "NHAN" << "\n";
		break;
	}
	case CHIA: {
		std::cout << "CHIA" << "\n";
		break;
	}


	}// end of switch statement
}