#include "Point.h"
#include "Constants.h"
#include <GL\freeglut.h>

Point::Point(int x, int y)
{
	this->x = x;
	this->y = y;
}

Point Point::midpoint(Point a, Point b)
{
	int x = (a.x + b.x) / 2;
	int y = (a.y + b.y) / 2;
	return Point(x,y);
}

void Point::draw()
{
	glVertex2f(x, y);
}

bool Point::isInside()
{
	if (this->x > 0 && this->x < windows_width && this->y > 0 && this->y < windows_height)
		return true;
	return false;
}

